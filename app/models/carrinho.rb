class Carrinho < ApplicationRecord
	has_many:itens, dependent: :destroy

	

	def add_instrumento(instumento)
		current_item = itens.find_by(instumento_id: instumento_id)
		if current_item
			current_item.increment(:quantity)
		else
			current_item = itens.build(instumento_id: instumento_id)
		end
		current_item
	end
	
end
