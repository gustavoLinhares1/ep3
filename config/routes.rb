Rails.application.routes.draw do
  resources :itens
  resources :carrinhos
  resources :instrumentos
  devise_for :users, controller: {
  	registrations:  'registrations'
  }
  root 'instrumentos#index'
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
