class AddQuantidadeParaItens < ActiveRecord::Migration[6.0]
  def change
  	add_column :itens, :quantity, :integer, default: 1
  end
end
