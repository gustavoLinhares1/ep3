class CreateItens < ActiveRecord::Migration[6.0]
  def change
    create_table :itens do |t|
      t.references :instrumento, null: false, foreign_key: true
      t.belongs_to :carrinho, foreign_key: true

      t.timestamps
    end
  end
end
