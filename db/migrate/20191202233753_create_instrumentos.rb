class CreateInstrumentos < ActiveRecord::Migration[6.0]
  def change
    create_table :instrumentos do |t|
      t.string :marca
      t.string :modelo
      t.text :descricao
      t.string :condicao
      t.string :titulo
      t.decimal :preco, precision: 5, scale: 2, default: 0

      t.timestamps
    end
  end
end
